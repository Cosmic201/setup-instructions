# Reverting files to their original state

Another great thing about git is that you can easily use it to get rid of any changes to files that you don't want. For example, sometimes you'll be editing shapefiles for your homework, and there's a chance you'll mess it up and need to revert the shapefile back to its original state. These steps will work on any file that's in a git repository, not just shapefiles. When you do it, the file goes back to the state it was in when it was last committed to the repository. **This doesn't help you get back to an intermediate point, only commits.**

1. Make sure that the files aren't open in ArcGIS or Python.
2. Open GitHub Desktop. 
3. Make sure the class materials (python-gis) repo is selected and then go to the Changes tab if you're not already there. 
4. You'll see the `.dbf` files for the edited shapefiles in the list of changed files (you'll possibly see other parts of the shapefile, too, but the `.dbf` is the most likely). 
5. Right-click on each one (hold down the control key to get them all at once like the screenshot) and select the option to Discard changes. That's it! 


![discard](images/github-discard.png)
