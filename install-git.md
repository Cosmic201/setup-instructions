# Install Git tools

We're going to use Git for course materials and homework, partly because I hate Canvas, and partly because it's something you should be familiar with if you're going to write code.

## Git for Windows

You need to install Git for Windows in order for one of the other tools we're using to work correctly (nbdime, which I'll show you probably the second week of class). [Download the installer](https://git-scm.com/download/win) and then you can just accept the defaults when installing it.

## GitHub Desktop

Although you could use Git for Windows to access the course materials and turn in your homework, [GitHub Desktop](https://desktop.github.com/) is a much nicer experience and it's what we'll be using in the lab.

**Skip the step to sign into GitHub** when installing it, because we're not using github.com. 

![skip signin](images/github-skip.png)

Fill in your name and email address when it asks. 

![info](images/github-info.png)
