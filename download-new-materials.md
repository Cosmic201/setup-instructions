# Download new materials

Each week you'll have to *pull* the new course materials from GitLab. It's pretty easy to do.

1. Open GitHub Desktop and make sure the `python-gis` repository is selected in the upper left and then hit the `Fetch origin` button. This tells GitHub Desktop to check for any changes on the server.  
![fetch](images/github-fetch.png)

2. If there are changes on the server, then the `Fetch origin` button will change to `Pull origin` and you'll see a number showing how many commits (snapshots) are on the server that aren't on your local copy.  
![pull](images/github-pull.png)

3. Hit the `Pull origin` button and wait for it to do its thing. If all goes well then it won't really seem like anything happened, except the button will switch back to saying `Fetch origin` again. But check your `python-gis` folder on your computer, and there will be a bunch of new files in there. That's it!

If you really hate the blank screen in GitHub Desktop, you can switch to the History tab and see all of the commits for the repository. That'll show you what I added recently.

![history](images/github-history.png)


