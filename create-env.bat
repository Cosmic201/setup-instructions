CALL conda create --name python-class --clone arcgispro-py3
CALL activate python-class
CALL pip install nbdime
CALL pip install jupyter_contrib_nbextensions
CALL jupyter contrib nbextension install --sys-prefix
CALL jupyter nbextension enable collapsible_headings/main
CALL jupyter nbextension enable scratchpad/main
CALL jupyter nbextension enable highlighter/highlighter
CALL jupyter nbextension enable toc2/main
CALL jupyter nbextension enable varInspector/main
CALL conda install -c defaults -c esri --override-channels -y geopandas bokeh
ECHO Done!
