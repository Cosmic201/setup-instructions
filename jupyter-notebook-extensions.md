# Jupyter notebook extensions

The lab has a whole bunch of extensions installed for Jupyter Notebook. I had Wes set it up so that some of them are turned on by default, but most are not. If you want to see what's available, hit the Nbextensions link at the top of the main notebook page.

![menu](images/jupyter-menu.png)

## Extensions enabled by default

Here are the ones that should already be turned on:

#### Table of contents

![toc](images/toc.png)

This turns the notebook table of contents on and off.

#### Highlighter

![highlight](images/highlight.png)

Use this to highlight important text in the notebook, just like highlighting in a textbook.

#### Variable inspector

![vars](images/vars.png)

This opens a panel that shows all of the variables in memory and their values.

#### Collapsible headings

![collapse](images/collapse.png)

Use this to minimize a section of the notebook.

#### Scratchpad

![scratchpad](images/scratchpad.png)

This icon is in the lower right corner of your notebook, and opens a window where you can type and run Python code without putting it in your notebook. This is a good way to test something without cluttering up your notebook.

#### Nbdime

This one isn't actually on by default as I write this, but I'll have Wes make the change. To turn it on, go the nbextensions menu and turn on `nbdime/index`.

![nbdime-saved](images/nbdime-saved.png)

This'll open a new tab that shows you the differences between the current open notebook and the last Jupyter checkpoint (so basically the changes since the last time you saved it).

![nbdime-git](images/nbdime-git.png)

This'll open a new tab that shows you the differences between the **saved** version of the current notebook and the last version committed with git. You could use this to see what changes you made to one of the notebooks I gave you, or to keep track of changes you make to your own notebooks between commits.




